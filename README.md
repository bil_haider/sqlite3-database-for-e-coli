# Sqlite3 database for coevolving groups predicted on E. coli branch

We used the output files of CoMap and generated a database to short-list candidate groups. These python scripts were used to parse output files of CoMap. Please follow the steps in readme.txt file to generate the database.